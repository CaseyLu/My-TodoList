import React, {Component} from 'react';

class TodoItem extends Component {
    
    constructor(props){
        super(props);
        this.deleteList = this.deleteList.bind(this);
        this.handleTodo = this.handleTodo.bind(this);
    }

    deleteList(){
        // 使用父组件的方法
        this.props.delete(this.props.index);
    }

    handleTodo(){
        // 调用父组件的方法，处理todo-item
        this.props.handle(this.props.index);
    }

    render(){
        const content = this.props.content;
        return(
            <div className="item">    
              <div 
                className="content"
                onClick={this.handleTodo}>
                {content}
              </div>
              <button
                className="del-btn"
                onClick={this.deleteList}>
                delete
              </button>  
            </div>
        )
    }
}

export default TodoItem;