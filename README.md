# My TodoList

#### 介绍
学习React的一个Demo，自己写了下样式和基本逻辑，主要实现的功能是：
- 添加todo事项
- 处理todo事项
- 删除todo或者done事项

#### 使用说明

精简了原始的React项目,实现了列表项的组件化，样式文件都在style.css中

- 点击add可以添加todo事件
- 点击todo事件内容，表示事件已完成，将进入done事件列表
- 点击del按钮删除事件

#### 安装运行

1.  `git clone https://gitee.com/CaseyLu/My-TodoList.git`
2.  `npm run build`
3.  `serve -s build`

之后，访问`localhost:5000`就可以查看本项目

或者直接使用`npm start`

#### 环境说明

1.  安装node环境(本机环境：node 10.15.3; npm 6.4.1; react 16.4)
2.  `npm install -g create-react-app`
3.  配置淘宝镜像加速react环境配置
    - `npm install -g cnpm --registry=https://registry.npm.taobao.org`
    - `npm config set registry https://registry.npm.taobao.org`
    - (检查是否成功配置 `npm config get registry`或者`npm info express`)
